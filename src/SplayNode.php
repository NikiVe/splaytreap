<?php


namespace DataStructure;


class SplayNode
{
    protected ?SplayNode $l = null;
    protected ?SplayNode $r = null;
    protected ?SplayNode $parrent = null;

    protected SplayTree $tree;

    protected $key = null;

    public function __construct($x, $tree) {
        $this->key = $x;
        $this->tree = $tree;
    }

    public function getKey() {
        return $this->key;
    }

    public function insert(SplayNode $node): bool {
        if ($node->key < $this->key) {
            if ($this->l === null) {
                $this->addLeft($node);
                return true;
            }

            return $this->l->insert($node);
        }

        if ($node->key > $this->key) {
            if ($this->r === null) {
                $this->addRight($node);
                return true;
            }

            return $this->r->insert($node);
        }

        return false;
    }

    public function setParrent(SplayNode $node) {
        $this->parrent = $node;
        return $this;
    }

    public function addLeft(SplayNode $node): SplayNode {
        $this->l = $node;
        $this->l->setParrent($this);
        return $this;
    }

    public function addRight(SplayNode $node) : SplayNode {
        $this->r = $node;
        $this->r->setParrent($this);
        return $this;
    }
    public function smallRightRotation() {
        if ($this->parrent) {
            // Открепить отца у его отца и сохранить в переменной
            $pNode = $this->parrent;
            if (isset($this->parrent->parrent)) {
                $ppNode = $this->parrent->parrent;
                $ppNode->detach($pNode);
            }

            // Открепить текущий элемент от отца
            $this->parrent->detach($this);

            // Если есть правый элемент, то сохранить ссылку на него и открепить его
            if ($rNode = $this->r) {
                $this->detach($this->r);
            }

            // Вставить отца
            $this->insert($pNode);
            // Если отец был Рутом, то стать вместо него рутом
            if ($this->tree->getRoot() === $pNode) {
                $this->tree->setRootNode($this);
            }
            // Если был правый элемент, то его вставить в новый правый элемент
            if (isset($rNode)){
                $this->r->insert($rNode);
            }

            // Вставить нас в прадеда
            if (isset($ppNode)) {
                $ppNode->insert($this);
            }
            return $this;
        }
        throw new \RuntimeException('Невозможно выполнить правый поворот');
    }

    public function smallLeftRotation() {
        if ($this->parrent) {
            $pNode = $this->parrent;

            if (isset($this->parrent->parrent)) {
                $ppNode = $this->parrent->parrent;
                $ppNode->detach($pNode);
            }
            $this->parrent->detach($this);

            if ($lNode = $this->l) {
                $this->detach($this->l);
            }

            // Если отец был Рутом, то стать вместо него рутом
            if ($this->tree->getRoot() === $pNode) {
                $this->tree->setRootNode($this);
            }
            $this->insert($pNode);
            if (isset($lNode)) {
                $this->l->insert($lNode);
            }

            if (isset($ppNode)) {
                $ppNode->insert($this);
            }
            return $this;
        }
        throw new \RuntimeException('Невозможно выполнить левый поворот');
    }

    public function detach(SplayNode $node) {
        if ($this->l === $node) {
            $this->l->unsetParent();
            $this->l = null;
        }
        if ($this->r === $node) {
            $this->r->unsetParent();
            $this->r = null;
        }
    }

    public function unsetParent() {
        $this->parrent = null;
    }

    public function search(int $key) {
        if ($this->key === $key) {
            return $this;
        }
        if ($this->key === null) {
            return false;
        }

        if ($key < $this->key) {
            if ($this->l === null) {
                return false;
            }

            return $this->l->search($key);
        }

        if ($key > $this->key) {
            if ($this->r === null) {
                return false;
            }

            return $this->r->search($key);
        }
        return false;
    }
    public function isLeft() {
        if ($this->parrent->l === $this) {
            return true;
        }
        if ($this->parrent->r === $this) {
            return false;
        }
        throw new \RuntimeException('oooops');
    }

    public function isRight() {
        return !$this->isLeft();
    }
}