<?php


namespace DataStructure;


class SplayTree
{
    public ?SplayNode $root = null;

    public function add($value) {
        if ($this->root === null) {
            $this->root = new SplayNode($value, $this);
        } else {
            $this->root->insert(new SplayNode($value, $this));
        }
    }

    public function getNode() : SplayNode {
        return $this->root;
    }

    public function setRootNode(SplayNode $node) {
        $this->root = $node;
    }

    public function search($value) {
        $node = $this->root->search($value);
        while ($node !== $this->root) {
            $node->isLeft() ? $node->smallRightRotation() : $node->smallLeftRotation();
        }
        return $node;
    }

    public function getRoot() {
        return $this->root;
    }
}