<?php


namespace DataStructure;


use phpDocumentor\Reflection\DocBlock\Tags\Deprecated;

class Tree
{
    protected Treap $root;
    protected $max = 1000;

    public function add($x) {
        $l = null;
        $r = null;

        $y = random_int(0, $this->max);

        if (!isset($this->root)) {
            $this->root = new Treap($x, $y);
            return;
        }
        $this->root->split($x, $l, $r);
        $n = new Treap($x, $y);
        $this->root = Treap::merge(Treap::merge($l, $n), $r);
    }

    public function split($x) {
        $l = null;
        $r = null;
        $this->root->split($x, $l, $r);
    }

    public function search($x) {
        return $this->root->search($x);
    }

}