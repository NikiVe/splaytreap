<?php


namespace DataStructure;


class Treap
{
    public int $x;
    public int $y;


    public ?Treap $left = null;
    public ?Treap $right = null;

    public function __construct(int $x, int $y, Treap $left = null, Treap $right = null) {
        $this->x = $x;
        $this->y = $y;
        $this->left = $left;
        $this->right = $right;
    }

    public function addAsHeap(Treap $treap) {
        if ($this->left && $this->right) {
            if ($this->x < $treap->x) {
                $this->right->addAsHeap($treap);
            } else {
                $this->left->addAsHeap($treap);
            }
            return;
        }
        if ($this->right) {
            $this->left = $treap;
            return;
        }
        $this->right = $treap;
    }

    public function split($x, &$l, &$r) {
        $nt = null;
        if ($x > $this->x) {
            if ($this->right) {
                $this->right->split($x, $l, $r);
            }
            $l = new Treap($this->x, $this->y, $this->left, $l);
        } else {
            if ($this->left) {
                $this->left->split($x, $l, $r);
            }
            $r = new Treap($this->x, $this->y, $r, $this->right);
        }
    }

    public static function merge(?Treap $l, ?Treap $r) : Treap {
        if ($l === null) {
            return $r;
        }
        if ($r === null) {
            return $l;
        }

        if ($l->y > $r->y) {
            $n = self::merge($l->right, $r);
            return new Treap($l->x, $l->y, $l->left, $n);
        }
        $n = self::merge($l, $r->left);
        return new Treap($r->x, $r->y, $n, $r->right);
    }

    public function search(int $x) {
        if ($this->x === $x) {
            return $this;
        }
        if ($this->x === null) {
            return false;
        }

        if ($x < $this->x) {
            if ($this->left === null) {
                return false;
            }

            return $this->left->search($x);
        }

        if ($x > $this->x) {
            if ($this->right === null) {
                return false;
            }

            return $this->right->search($x);
        }
        return false;
    }
}